# MCurl

Simple and powerful wrapper for `curl_multi_*`

## Install

download
composer
git clone

## Usage

	<?php

	$threads = 20;
	$options = array(
		'followlocation'  =>  true,  //CURLOPT_FOLLOWLOCATION
		'nobody'          =>  true,  //CURLOPT_NOBODY
	);

	$queue[] = array(
		'url'             =>  'http://example1.com',  //CURLOPT_URL
		'connecttimeout'  =>  20,                     //CURLOPT_CONNECTTIMEOUT
	);
	$queue[] = array(
		'url'             =>  'http://example2.com',  //CURLOPT_URL
		'connecttimeout'  =>  10,                     //CURLOPT_CONNECTTIMEOUT
	);

	$mcurl = new MCurl\Curl($options, $queue, $threads);
	while ($response = $mcurl->exec()) {
		$message = "URL: $response->url\n";
		if ($response->error) {
			$message .= "ERROR: $response->error\n";
		} else {
			$message .= "RESPONSE CODE: $response->code\n";
			$message .= "RESPONSE BODY: $response->data\n";
		}
		echo "$message\n";
	}

## Options

Mcurl options is a array of [curl options][1], but in lowercase and without `CURLOPT_` prefix.

This options is defaults for all requests.

	<?php

	$options = array(
		'connecttimeout'  =>  10,  //CURLOPT_CONNECTTIMEOUT
	);

	$mcurl = new MCurl\Curl($options);

	//or

	$mcurl = new MCurl\Curl;
	$mcurl->options = $options;

[1]: http://php.net/manual/en/function.curl-setopt.php

## Queue

MCurl queue is a php array with curl options.

This options is specific for each requests.

	<?php
	
	$queue[] = array(
		'url'             =>  'http://example1.com',  //CURLOPT_URL
		'followlocation'  =>  true,                   //CURLOPT_FOLLOWLOCATION
	);

	$queue[] = array(
		'url'             =>  'http://example2.com',  //CURLOPT_URL
		'followlocation'  =>  false,                  //CURLOPT_FOLLOWLOCATION
		'nobody'          =>  true,                   //CURLOPT_NOBODY
	);

	$defaultOptions = array();

	$mcurl = new MCurl\Curl($defaultOptions, $queue);

	//or

	$mcurl = new MCurl\Curl;
	$mcurl->queue = $queue;

## Threads

Threads is a count of parallel connections used by curl.

	<?php

	$threads = 20;

	$mcurl = new MCurl\Curl($options, $queue, $threads);

	//or

	$mcurl = new MCurl\Curl;
	$mcurl->threads = $threads;

## Busy loop

add new urls

## Response
